# Ansible Role: NetBox

An Ansible Role that installs on Debian/Ubuntu.

This role install all dependencies required by NetBox including the PostgreSQL
database. So it can be used to setup a NetBox appliance including everything in
the same machine.

Web backend and frontend setups can be disabled if you already have your own
way to handle them.

## Dependencies

None.

## Roles Variables

Available variables are listed below, along with default values:

Setup for the PostgreSQL database:

    netbox_database: netbox
    netbox_database_user: netbox
    netbox_database_password: netbox
    netbox_database_host: localhost # This will force PostgreSQL to be setup

Where to get NetBox and which version:

    netbox_version: v3.4.2
    netbox_git_url: https://github.com/netbox-community/netbox.git

Where to install NetBox:

    netbox_install_directory: /opt/netbox

The username, password and email for the super user.

    netbox_superuser_username: admin
    netbox_superuser_password: admin
    netbox_superuser_email: admin@example.com

LDAP can be used as authentication mechanism. It must be enabled, and the whole
LDAP configuration has to be provided in the following variables (see NetBox
[documentation](https://netbox.readthedocs.io/en/stable/installation/6-ldap/)):

    netbox_setup_ldap_auth: false
    netbox_ldap_config: ""

Other Python packages can be installed using `local_requirements.txt`, this is
useful to install packages such as NAPALM or plugins:

    netbox_local_requirements:
      - napalm
      …

The configuration for NetBox must be given as `key: value` pairs like the
following, please note that the secret key does not need to be given as it will
be generated automatically:

    netbox_config:
      ALLOWED_HOSTS:
        - localhost
        - 127.0.0.1
      TIME_ZONE: "Europe/Paris"
      …

Housekeeping to clear expired sessions and remove old changelog entries can be
configured with the following variables:

    netbox_housekeeping: true
    netbox_housekeeping_hour: 4
    netbox_housekeeping_minute: 30

Configuration for the backend web server and systemd:

    netbox_setup_systemd: false
    netbox_gunicorn_address: 127.0.0.1
    netbox_gunicorn_port: 8001
    netbox_gunicorn_workers_number: 5

Whether or not to configure the frontend web server:

    netbox_setup_web_frontend: false

Whether a ssl certificate should be copied from the Ansible control node to the remote hosts or an existing certificate is already present on the remote host (Choose one of theese methods or ommit them to use http only):

    # Examples (Choose one of theese two methods)
    copy_netbox_ssl_certificate: "<inventory_path>/group_vars/secrets/netbox.test.2ln.mueller.de.crt"
    copy_netbox_ssl_certificate_key: "<inventory_path>/group_vars/secrets/netbox.test.2ln.mueller.de.key"

    existing_netbox_ssl_certificate: "/etc/step/certs/cert.crt"
    existing_netbox_ssl_certificate_key: "/etc/step/secrets/cert.key"    

Redis can be installed on localhost or connect to existing node:

    netbox_redis_host: '' # set to localhost to install redis locally
    netbox_redis_port: 6379

An existing redis high available setup with redis-sentinel can be used by providing the following variables (Installation of redis cluster or sentinel is not part of this role!):

    netbox_redis_sentinels: 
      - { host: '192.168.0.1', port: '26379' }
      - { host: '192.168.0.2', port: '26379' }
    netbox_redis_sentinel_service: 'netbox' # Required: Name of Redis master service in sentinel config
    netbox_redis_username: '' # Optional 
    netbox_redis_password: '' # Optional 
    netbox_redis_default_timeout: 10
    netbox_redis_ssl_enabled: false

By default all media content that is uploaded to Netbox (images, scripts etc.) will be stored locally in the `netbox_install_directory`.
In a high available setup you would use a shared S3 storage backend (Minio S3, AWS):

    netbox_media_storage_s3_config:
      aws_access_key_id: ''
      aws_secret_access_key: ''
      aws_storage_bucket_name: ''
      aws_s3_region_name: ''
      aws_s3_endpoint_url: ''
      aws_s3_verify: '' # Optional: Path to internal CA chain when using SSL Intercept

## Example Playbook

    - hosts: netboxes
      roles:
        - { role: gmazoyer.netbox }

## License

This Ansible Role is released under the terms of the GNU GPLv3. Please read
the `LICENSE` file for more information.

Portions of this role include MIT-licensed code (see `7c400dd`) and are
demarcated in the header. See `LICENSE-MIT` for more information.

## Author Information

Oleg Franko: Modified and extended role with new features and high available setups
This role was originally created in 2017 by [Guillaume Mazoyer](https://mazoyer.eu).
